import pytest

from tests.CoreTestCase import CoreTestCase


class TestLogin(CoreTestCase):

    @pytest.mark.test
    def test_login_page_is_displayed(self, init_pages):
        Page = init_pages
        Page.LoginPage.checkloaded()

    @pytest.mark.test
    def test_click_on_login(self, init_pages):
        Page = init_pages
        Page.LoginPage.clickStart()

