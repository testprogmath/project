import pytest
from core.main.configs.Config import config_object
from core.main.core.Platform import platform_object
from core.main.pages.Pages import Pages

@pytest.fixture()
def init_driver():
    driver = platform_object.getDriver(config_object.getDeviceFromProperties(),
                                       server_url=config_object.APPIUM_SERVER_ADDRESS)
    driver.init_webdriver()
    yield driver
    driver.quit()


@pytest.fixture
def init_pages(init_driver):
    return Pages(init_driver)


