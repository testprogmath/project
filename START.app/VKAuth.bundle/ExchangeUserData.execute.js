var response = API.auth.getExchangeToken({"create_common_token": 1, "create_tier_tokens": 1});

response.user = API.users.get({"user_id": Args.user_id, "fields": "photo_200"})[0];

return response;
