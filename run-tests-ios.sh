#!/bin/bash

echo "Installing dependencies"
chmod 0755 requirements.txt

python3 -m pip install -r requirements.txt
appium plugin install images 
appium plugin install execute-driver


echo "Starting Appium ..."
appium --use-plugins="images, execute-driver" -a 127.0.0.1 -p 4723 &
sleep 10
ps -ef|grep appium


## Desired capabilities:
export PLATFORM="ios"
export APP_IOS_PATH="./START.app"
export APPIUM_URL="http://localhost:4723/wd/hub"
export APPIUM_DEVICE="Local Device"
export APPIUM_AUTOMATION="XCUITest"


## Run the test:
echo "Running tests"

rm -rf screenshots
python3 -m pytest tests

echo "Tests done"
