from decouple import config
from core.main.models.Device import Device
from core.resources.deviceAndroid import device_1
from core.resources.deviceIos import iphone13


class _Config:
    APP_ANDROID_PATH = config('APP_ANDROID_PATH')
    APP_IOS_PATH = config('APP_IOS_PATH')
    APPIUM_SERVER_ADDRESS = config('APPIUM_SERVER_ADDRESS')
    PLATFORM = config('PLATFORM')
    TIMEOUT = config('TIMEOUT')

    def getDeviceFromProperties(self):
        if self.PLATFORM == 'android':
            return Device(platform_version=device_1["platformVersion"],
                              device_name=device_1["deviceName"],
                              avd_name=device_1["avdName"])
        elif self.PLATFORM == 'ios':
            return Device(platform_version=iphone13["platformVersion"],
                          device_name=iphone13["deviceName"],
                          avd_name="")


config_object = _Config()
