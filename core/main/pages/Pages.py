from core.main.configs.Config import config_object
from core.main.core.Platform import platform_object
from core.main.pages.login_page.AndroidLoginPage import AndroidLoginPage_object
from core.main.pages.login_page.IOSLoginPage import IOSLoginPage_object
from core.main.pages.login_page.LoginPage import (LoginPage)


def get_locators_for_platform(locators_dict):
    if platform_object.isIos():
        return locators_dict['IOS']
    elif platform_object.isAndroid():
        return locators_dict['ANDROID']
    else:
        raise Exception(f"Cannot find locators for platform. Platform value: {config_object.PLATFORM}")


class Pages:
    LoginPageLocators = {'IOS': IOSLoginPage_object, 'ANDROID': AndroidLoginPage_object}

    def __init__(self, driver):
        self.driver = driver
        self.LoginPage = LoginPage(locators=get_locators_for_platform(self.LoginPageLocators), driver=self.driver)
