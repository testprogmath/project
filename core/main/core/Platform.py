from core.main.configs.Config import config_object
from core.main.models.Device import Device
from core.main.fixtures.Driver import Driver


class Platform:
    PLATFORM_IOS = 'ios'
    PLATFORM_ANDROID = 'android'

    def isIos(self) -> bool:
        return config_object.PLATFORM == self.PLATFORM_IOS

    def isAndroid(self) -> bool:
        return config_object.PLATFORM == self.PLATFORM_ANDROID

    def getDriver(self, device, server_url) -> Driver:
        if self.isIos():
            desiredCaps = self.getDesiredCapsIos(device)
        elif self.isAndroid():
            desiredCaps = self.getDesiredCapsAndroid(device)
        else:
            raise Exception(f"Cannot detect type of the Driver. Platform value: {config_object.PLATFORM}")

        return Driver(server_url=server_url, desired_caps=desiredCaps)

    def getDesiredCapsAndroid(self, device: Device):
        capabilities = {}
        capabilities["platformName"] = 'Android'
        capabilities["deviceName"] = device.device_name
        capabilities["platformVersion"] = device.platform_version
        capabilities["automationName"] = "uiautomator2"
        capabilities["app"] = config_object.APP_ANDROID_PATH
        return capabilities

    def getDesiredCapsIos(self, device):
        capabilities = {}
        capabilities["platformName"] = 'iOS'
        capabilities["deviceName"] = device.device_name
        capabilities["platformVersion"] = device.platform_version
        capabilities["app"] = config_object.APP_IOS_PATH
        capabilities["udid"] = 'A60624D1-932B-47C6-8B50-3DBC3262B7A0'
        return capabilities



platform_object = Platform()
