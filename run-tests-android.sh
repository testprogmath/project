echo "Installing dependencies"
chmod 0755 requirements.txt
npm install -g appium
npm install wd
python3 -m pip install -r requirements.txt --cache-dir .cache/pip
python3 -m pip install pytest
appium driver install uiautomator2
appium plugin install images 
appium plugin install execute-driver


echo "Starting Appium ..."
appium --use-plugins="images, execute-driver" -a 127.0.0.1 -p 4723 &
echo "running in background"
sleep 10
#ps -ef|grep appium
    
export APP_ANDROID_PATH=./app-debug.apk #App file is at current working folder

## Desired capabilities:
export APPIUM_SERVER_ADDRESS="http://127.0.0.1:4723"
export APPIUM_DEVICE="Local Device"
export APPIUM_PLATFORM="android"
export APPIUM_AUTOMATION="uiautomator2"

## Run the test:
echo "Running tests"

python3 -m pytest

echo "Tests done"
